var gulp = require('gulp'),
    twig = require('gulp-twig'),
    gulpif = require('gulp-if'),
    browserSync = require('browser-sync'),
    plumber = require('gulp-plumber'),
    util = require('gulp-util'),
    del = require('del');

gulp.task('templates', function() {
  gulp.src(settings.templates.src + '*.{twig,html}')
    .pipe(plumber(function(error) {
      util.log(util.colors.red('Error (' + error.plugin + '): ' + error.message));
      this.emit('end');
    }))
    .pipe(gulpif(settings.templates.build, twig({
      base: settings.templates.src
    })))
  .pipe(gulpif(settings.templates.build, gulp.dest(settings.templates.dist)))
  .pipe(browserSync.reload({
    stream: true
  }));
});
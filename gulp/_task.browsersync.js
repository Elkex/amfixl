var gulp = require('gulp'),
    browserSync = require('browser-sync');

gulp.task('browsersync', function() {
  browserSync.init({
    proxy: settings.browserSync.url
  });
});
var gulp = require('gulp'),
    browserSync = require('browser-sync'),
    sprite = require('gulp-svg-sprite'),
    plumber = require('gulp-plumber'),
    util = require('gulp-util');

gulp.task('icons', function(){
  gulp.src(settings.icons.src + '*.svg')
    .pipe(plumber(function(error) {
      util.log(util.colors.red('Error (' + error.plugin + '): ' + error.message));
      this.emit('end');
    }))
    .pipe(sprite({
      mode: {
        symbol: {
          dest: './',
          sprite: settings.icons.filename,
        }
      },
      shape: {
        id: {
          generator: '%s'
        }
      },
      svg: {
        xmlDeclaration: false,
        doctypeDeclaration: false
      }
    }))
  .pipe(gulp.dest(settings.icons.dist))
  .pipe(browserSync.reload({
    stream: true
  }));
});
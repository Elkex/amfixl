var gulp = require('gulp');

gulp.task('watch', function() {
  gulp.watch(settings.templates.src + '**/*.{twig,html}', ['templates']);
  gulp.watch(settings.icons.src + '*.svg', ['icons']);
  gulp.watch(settings.sass.src + '**/*.scss', ['sass']);
  gulp.watch(settings.vendor.src + '**/*', ['vendor']);
  gulp.watch([settings.js.components + '*.js', settings.js.src + '*.js'], ['js']);
  gulp.watch(settings.images.src + '**/*.{jpeg,jpg,png,gif,svg}', ['images']);
  gulp.watch(settings.fonts.src + '*.{ttf,otf,eot,svg,woff,woff2}', ['fonts']);
});
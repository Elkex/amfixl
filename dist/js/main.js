var masonry = {
    init : function() {
        $('.js-masonry').masonry();
    }
}
nav = {
    init: function () {
        $('.c-nav-toggle').click(function(){
            console.log("clicked");
            $('.c-nav-toggle').toggleClass('is-open');
            $('.c-nav-menu').toggleClass('is-open');
            $('.c-nav').toggleClass('is-open');
            $('body').toggleClass('is-open');
        });
    }
}
slider = {
    init: function () {
        $('.responsive').slick({
            dots: true,
            infinite: false,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }
}
$(document).ready(function($) {
    nav.init();
    slider.init();
});
//# sourceMappingURL=main.js.map

nav = {
    init: function () {
        $('.c-nav-toggle').click(function(){
            console.log("clicked");
            $('.c-nav-toggle').toggleClass('is-open');
            $('.c-nav-menu').toggleClass('is-open');
            $('.c-nav').toggleClass('is-open');
            $('body').toggleClass('is-open');
        });
    }
}